from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    """Класс для расширения стандартного набора полей пользователя по уполчанию"""

    user = models.OneToOneField(
        User,
        on_delete=models.CASCADE,
        verbose_name = 'Пользователь',
        verbose_name_plural = 'Пользователи',
    )
    location = models.CharField(
        max_length=31,
        blank=True,
        verbose_name='Место жительства',
        verbose_name_plural='Места жительства',
    )
    birth_date = models.DateField(
        null=True,
        blank=True,
        verbose_name='Дата рождения',
        verbose_name_plural='Даты рождения',
    )

    def __str__(self):
        return self.user.username


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
